//
//  viewTwo.m
//  QuizApp1
//
//  Created by twneff on 10/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "viewTwo.h"


@implementation viewTwo

@synthesize subject;



-(IBAction)readButtonPressed{
    /* NEED TO DETERMINE TYPE OF viewTwo and edit code in here as necessary
     this is currently implemented as a picker view                       */
    
    QuestionView *qV = [[[QuestionView alloc] initWithNibName:@"QuestionView" bundle:nil] autorelease];
    int index = [picker2 selectedRowInComponent:0];
    qV.dif = [diff objectAtIndex:index];
    qV.qSubject = subject;          
    [self.navigationController pushViewController:qV animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    diff = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"difficulty" ofType:@"plist"]];
    [diff retain];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    return [diff count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    return [diff objectAtIndex:row];
}

@end
