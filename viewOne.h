//
//  viewOne.h
//  QuizApp1
//
//  Created by twneff on 10/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  This view will be the controller for the Subject Selection View:
//  It is desired that this view will ask the user to select a subject
//  from the picker and click the button to "Begin the Game"
//

#import <UIKit/UIKit.h>
#import "viewTwo.h"

@interface viewOne : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>{
    NSArray *subjects;
    UIPickerView *picker;
    
}
@property (nonatomic, retain) IBOutlet UIPickerView *picker;
-(IBAction)readButtonPressed;
@end

