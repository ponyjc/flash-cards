//
//  QuestionView.h
//  QuizApp1
//
//  Created by enowick1 on 10/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.

//Question View will load the question to the view controller
//

#import <UIKit/UIKit.h>


@interface QuestionView : UIViewController <UITableViewDataSource,UITableViewDelegate>{
    UITextView *questionTextView;
    NSString *dif;
    NSString *qSubject;
    NSArray *questions;
    NSArray *answers;
    NSInteger i;
}

@property(nonatomic,retain) IBOutlet UITextView *questionTextView;
@property(nonatomic,retain) NSString *dif;
@property(nonatomic,retain) NSString *qSubject;
@property(nonatomic,retain) NSArray *questions;
@property(nonatomic,retain) NSArray *answers;
-(void)loadQuestion:(NSArray *)questions:(NSInteger)i;
-(void)loadAnswer:(NSArray *)questions:(NSInteger)i;
@end
