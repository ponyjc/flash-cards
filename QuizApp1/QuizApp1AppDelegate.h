//
//  QuizApp1AppDelegate.h
//  QuizApp1
//
//  Created by twneff on 10/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  This view will serve as the start screen:
//  It is desired that this screen be aesthetically pleasing to the 
//  user and provide a desire to the user to play the game using impressive gfx designz brah
//
#import <UIKit/UIKit.h>
#import "viewOne.h"


@interface QuizApp1AppDelegate : NSObject <UIApplicationDelegate> {
    UINavigationController *navControl;

}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navControl;

@end
