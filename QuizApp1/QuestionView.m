//
//  QuestionView.m
//  QuizApp1
//
//  Created by enowick1 on 10/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "QuestionView.h"


@implementation QuestionView
@synthesize questionTextView,dif,qSubject,questions;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];


    
    if([qSubject isEqualToString:@"Math"])
    {
        if([dif isEqualToString: @"easy"])
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"mathQuestionsE" ofType:@"plist"]];
            [questions retain];
                [self loadQuestion:questions:i];
                
        }
        else 
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"mathQuestionsH" ofType:@"plist"]];
            [questions retain];
            [self loadQuestion:questions:i];
            
        }
            
    }
    else
    {
        if([dif isEqualToString: @"easy"])
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"historyQuestionsE" ofType:@"plist"]];
                        [questions retain];
            [self loadQuestion:questions:i];
 
        }
        else 
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"historyQuestionsH" ofType:@"plist"]];
                        [questions retain];
            [self loadQuestion:questions:i];
                
        }
    }
    
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)loadQuestion:(NSArray *)questions: (NSInteger)i {
    
    NSString *textString = [questions objectAtIndex:i];
    // Set the text of our text view to the loaded string.
    self.questionTextView.text = textString;
    
}

-(void)loadAnswer:(NSArray *)answers: (NSInteger)i {
    
    NSString *textString = [answers objectAtIndex:i];
    // Set the text of our text view to the loaded string.
    self.questionTextView.text = textString;
    
}
-(IBAction) AnswerreadButtonPressed{
    /* NEED TO DETERMINE TYPE OF viewTwo and edit code in here as necessary
     this is currently implemented as a picker view                       */
    
    
    
    if([qSubject isEqualToString:@"Math"])
    {
        if([dif isEqualToString: @"easy"])
        {
            answers = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"mathAnswersE" ofType:@"plist"]];
                        [answers retain];
            [self loadAnswer:answers :i];
            
        }
        else 
        {
           answers = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"mathAnswersH" ofType:@"plist"]];
            [answers retain];

            [self loadAnswer:answers :i];
            
        }
        
    }
    else
    {
        if([dif isEqualToString: @"easy"])
        {
            answers = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"historyAnswersE" ofType:@"plist"]];
            [answers retain];

            [self loadAnswer:answers :i];
            
        }
        else 
        {
            answers = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"historyAnswersH" ofType:@"plist"]];
            [answers retain];

            [self loadAnswer:answers :i];
            
        }
    }

    
    
}
-(IBAction) nextreadButtonPressed{
    /* NEED TO DETERMINE TYPE OF viewTwo and edit code in here as necessary
     this is currently implemented as a picker view          */                
    if (i < ([questions count] -1))
    {

        i++;


    }
    else{
        i=0;

    }
    

    
    if([qSubject isEqualToString:@"Math"])
    {
        if([dif isEqualToString: @"easy"])
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"mathQuestionsE" ofType:@"plist"]];
                        [questions retain];
            [self loadQuestion:questions:i];
            
        }
        else 
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"mathQuestionsH" ofType:@"plist"]];
                        [questions retain];
            [self loadQuestion:questions:i];
            
        }
        
    }
    else
    {
        if([dif isEqualToString: @"easy"])
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"historyQuestionsE" ofType:@"plist"]];
                        [questions retain];
            [self loadQuestion:questions:i];
            
        }
        else 
        {
            questions = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"historyQuestionsH" ofType:@"plist"]];
                        [questions retain];
            [self loadQuestion:questions:i];
            
        }
    }
   
    
    
}
@end
