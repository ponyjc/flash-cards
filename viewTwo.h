//
//  viewTwo.h
//  QuizApp1
//
//  Created by twneff on 10/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
//  This view will be the controller for the Question View:
//  It is desired that this view will ask a question to the
//  user and provide choices for the user to pick as the answer
//
//


#import <UIKit/UIKit.h>
#import "QuestionView.h"


@interface viewTwo : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>{
    
    NSString* subject;
    NSArray *diff;
    UIPickerView *picker2;
        
    }

@property (nonatomic, retain) IBOutlet UIPickerView *picker2;
@property(nonatomic, retain) NSString *subject;
-(IBAction)readButtonPressed;
@end
