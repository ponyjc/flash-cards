//
//  viewOne.m
//  QuizApp1
//
//  Created by twneff on 10/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

//change

#import "viewOne.h"
#import "viewTwo.h"

@implementation viewOne
@synthesize picker;

-(IBAction)readButtonPressed{
    /* NEED TO DETERMINE TYPE OF viewTwo and edit code in here as necessary
       this is currently implemented as a picker view                       */
    
    viewTwo *v2 = [[[viewTwo alloc] initWithNibName:@"viewTwo" bundle:nil] autorelease];
    int index = [picker selectedRowInComponent:0];
    v2.subject = [subjects objectAtIndex:index];
    [self.navigationController pushViewController:v2 animated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    subjects = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"subjects" ofType:@"plist"]];
    [subjects retain];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component {
    return [subjects count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component {
    return [subjects objectAtIndex:row];
}

@end
